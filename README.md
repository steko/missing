missing
=======

You have a sequence of numbered items and want to check if any are missing,
which ones, how many gaps are found in the sequence, etc.

You're probably dealing with a museum inventory, an old paper log, an
historical archive and you just can't get rid of those numbers (perhaps
because there are cultural artifacts that have been marked with those
numbers as persistent identifiers in a pre-digital era) or you can't
pretend the gaps aren't there (because these are not simple DB unique keys).

This is my cornucopia of small functions to deal with this kind of problems.
Enjoy.
